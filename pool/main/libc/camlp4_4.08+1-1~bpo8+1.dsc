-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: camlp4
Binary: camlp4, libcamlp4-ocaml-dev
Architecture: any
Version: 4.08+1-1~bpo8+1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Stéphane Glondu <glondu@debian.org>, Ximin Luo <infinity0@debian.org>
Homepage: https://github.com/ocaml/camlp4
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/camlp4
Vcs-Git: https://salsa.debian.org/ocaml-team/camlp4.git
Build-Depends: debhelper (>= 10), ocaml-nox (>= 4.08), ocamlbuild (>= 0.10.1), dh-ocaml
Package-List:
 camlp4 deb ocaml optional arch=any
 libcamlp4-ocaml-dev deb ocaml optional arch=any
Checksums-Sha1:
 7cf80c5b340fcee29d00034003e66648fc66cc3e 649830 camlp4_4.08+1.orig.tar.gz
 c0a986e8fbc3f75323ad8dbb8cf224c5a1f066ba 2548 camlp4_4.08+1-1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 655cd3bdcafbf8435877f60f4b47dd2eb69feef5afd8881291ef01ba12bd9d88 649830 camlp4_4.08+1.orig.tar.gz
 26e9429b516d6c078ad766c66863a420c50545b2086c5bb9cb64f53f6f69db67 2548 camlp4_4.08+1-1~bpo8+1.debian.tar.xz
Files:
 7e09e32fdfb185050f69cb2f1e77bf8a 649830 camlp4_4.08+1.orig.tar.gz
 c3d0e78726f15191c4095e5591054bf2 2548 camlp4_4.08+1-1~bpo8+1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtVdVAAoJEJZbtTmX/ZH+VNcP/Ra25NU64QWJlbdg4fG4wbnP
JaFdHLyk2iwgbYMfDSnq1GyhNdX0EzI7RF+FXa1kerZz8aKS/C5MxpNZqeSxRFBP
bfO+sz2iS0p9Liazdm+3MKXcNoWhFXHAWvx1VKcoopSY/MAUM9BvBYdFou6KzO68
4Yq6iFJD0p2ePyF286z4CZkq+yMW6FzFN1GJne2jxrn/YtQquUGjJhJEDKnu/wlw
HCMV3dQau+BXEI0Q8X8Kx2Uy/du2UyyiEyGoHHbSa0H0Grvj2Y+pcOhQz6M0WOYU
8OTWkY5C5mZY1k9z7vQu7PahpxQhx5dWEpucASn4KhXC/Y9nfV3trV2LT4D9/U6N
OnAnMVPaH9O9M2E/LmRkFBoNZ4EPzR6/8zxz+vGyCC/4rzpVrX7BDtSvfmnVYAYJ
R/vavo5cHfrgXf+hco6Pi++RU4rX8UqmIbhFN1dO9SXRJOALIm/jWdP/THi8oRDd
D07r0QinF5IkcRfwgHpdWY2X+SHHF3CJrGjwDm9zq6NCZWHE4onbYS9T/dqzA7lb
/UQEnqcTAZPYw9f1WzdHHsD/BlZemnEx9PS+bi7EHOQHtKR1YlfkBfGJ8rDd9/GP
bMMeGZ/yS0G93fQhJP9weVvKrnlL7NRmjPqnDCnuYzue+q9V1XvRCIeUEQv98mKK
/lmOao4gYHVwKaTkxHRm
=b1BF
-----END PGP SIGNATURE-----
