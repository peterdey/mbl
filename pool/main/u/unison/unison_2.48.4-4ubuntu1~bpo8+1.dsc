-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: unison
Binary: unison, unison-gtk
Architecture: any
Version: 2.48.4-4ubuntu1~bpo8+1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Stéphane Glondu <glondu@debian.org>
Homepage: http://www.cis.upenn.edu/~bcpierce/unison/
Standards-Version: 4.1.1
Vcs-Browser: https://salsa.debian.org/ocaml-team/unison
Vcs-Git: https://salsa.debian.org/ocaml-team/unison.git
Build-Depends: cdbs (>= 0.4.23), debhelper (>= 9), ocaml-nox (>= 3.10.0-8), liblablgtk2-ocaml-dev (>= 2.12.0-2), chrpath, librsvg2-bin (>= 2.18.2), imagemagick, dh-ocaml (>= 0.9.1)
Package-List:
 unison deb utils optional arch=any
 unison-gtk deb utils optional arch=any
Checksums-Sha1:
 baeff0e3a942c710ce551cca118b891feca137c5 1200861 unison_2.48.4.orig.tar.gz
 7b95c4d79d43911d40422e03c5a31c0d82712358 20880 unison_2.48.4-4ubuntu1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 30aa53cd671d673580104f04be3cf81ac1e20a2e8baaf7274498739d59e99de8 1200861 unison_2.48.4.orig.tar.gz
 f4194dd9d1929d0b65ed353360569c7411025ebfb407dc9e2e7e51f4d897e712 20880 unison_2.48.4-4ubuntu1~bpo8+1.debian.tar.xz
Files:
 5334b78c7e68169df7de95f4c6c4b60f 1200861 unison_2.48.4.orig.tar.gz
 ba3483469c55aa5d5628a2727fb17264 20880 unison_2.48.4-4ubuntu1~bpo8+1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtY5YAAoJEJZbtTmX/ZH+Z6kP/iBEessyLA3cerY9opQDXgif
sw7kh61cg/xPl+5FlOACh2V1pPU9mhcDt/vTCV9b/Un9/DLjq0oqaLrtFwuBp+bs
bPE4LF5lBQyAO6JVXW66MHGYXgRlerGUga9W2vRN3eMIzRoG/Z64x5U7W1WSqjmU
wJ/rX63gx+vBTCbRmOSi9/Tb2+2z7gLZNkgRCVBQPo7ISFoddMBaHWrj+c7kZrRd
oMGw/k3s0jSd7ptuAM1ztxmIWu1SPmhM/bYIrpcUkJF+UQSY4pI6O3PrstYkg88b
gd6TRfUh7j1YYCHzbEHdG07OW8ASW3HtmYYGwP9PgEYZuvx38i57CVpunC0hOl1t
AgmlRu1rYJrnY2n/eqKw6p57rH5jnuF0lSfHKQyFR3X1YJ9foK3aCjmSlOYId/l9
pOH5gGBnNX7ZH6eTBwy6/j6YOW+b9lfNY8ZEuJEGziXmKsf6qe6sXgzufiB5/W4R
ZiXEACxnXHxKkqm+2dhEIXxvfd1mXAO43IVq4hNrBx2pKgcowfmWO2HfvlbQG8Px
RJwfsTjWTNG8J8MW3cscA40QLxPWxdT7bfFWlIhURcgbMvLLj3fdXo/Iu2fms2cW
Ze3WFkqdRAWKwAXK/zHl/o+orZ8e5jk1OaC3tNmUI5AewFcDEnpoQIXueAVcSg9H
9DlG6eNYj9dc0B3LCTCe
=hmRP
-----END PGP SIGNATURE-----
