-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: findlib
Binary: ocaml-findlib, libfindlib-ocaml-dev, libfindlib-ocaml
Architecture: any
Version: 1.8.1-1build3~bpo8+1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Mehdi Dogguy <mehdi@debian.org>, Stéphane Glondu <glondu@debian.org>, Ximin Luo <infinity0@debian.org>
Homepage: http://projects.camlcity.org/projects/findlib.html
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/findlib
Vcs-Git: https://salsa.debian.org/ocaml-team/findlib.git
Build-Depends: debhelper (>= 10), bash-completion, ocaml-nox (>= 4.03.0), ocaml, m4, gawk | awk, dh-ocaml (>= 0.9~)
Package-List:
 libfindlib-ocaml deb ocaml optional arch=any
 libfindlib-ocaml-dev deb ocaml optional arch=any
 ocaml-findlib deb ocaml optional arch=any
Checksums-Sha1:
 d3fe18a261c453176e67884b0b475c3a82b68397 261544 findlib_1.8.1.orig.tar.gz
 c6a3e5a2e5f67f1b771958ec47f930a5d751eac9 13740 findlib_1.8.1-1build3~bpo8+1.debian.tar.xz
Checksums-Sha256:
 8e85cfa57e8745715432df3116697c8f41cb24b5ec16d1d5acd25e0196d34303 261544 findlib_1.8.1.orig.tar.gz
 63ba2fdfc4d06ab23cecd3a95e888b5edbb3dcb8fbb9c1193b5eeb8c05022b4b 13740 findlib_1.8.1-1build3~bpo8+1.debian.tar.xz
Files:
 18ca650982c15536616dea0e422cbd8c 261544 findlib_1.8.1.orig.tar.gz
 889c462d964b61e49862cc1c71904821 13740 findlib_1.8.1-1build3~bpo8+1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtU4YAAoJEJZbtTmX/ZH+PtIP/RvOQMxnCxoeGHOS2hKbI4/U
sl6xXnw5UodMBdvrQeRZWib172tTIBLS9dpsLDz+M+M4nqLfaD8SeiLYJDbSNF5p
u5vBUcdugo3Nmj3uv8+M+ML2FIFZStYbIIeep4z8H26ZTbl45/nhMnPFT0Or2uRO
xOcY/JHxLASaOuUeoh382JtKYWuJr2R+t8XBZQMShWUqdAH4rbEhKDuLl++RMjn+
ITnvx7/YlQNc/f44mls6m77K/Jg+vx0Xf02Lb/6CQSWj+srLXIz1nogJ8OQwyW/t
aH2hVXUXaIlw0QRStsZofl0hiMyeETt6vureF3ZQh2oiIJhf1pmbKcM7oOl4Upet
20Y52z1ryyogsxxJdKoUdvP+b9obwAEGZYQd6C6vFraXvRqffIqUkqgby7flAei2
EU0+/QEGVJA9caZKy4zliyccA5SQYfTLNTNXeDq/K5y0CQR4PexVVKMSBjyl0yDC
l00X4LYUdqkPwcEPjfz2g3TBGOojCV6dBSfEnhcflMo1UhWvBV1kTjK9A+kpcIh4
61MvHAcwDqejzPYgHPEHc6SO7h6G+JVohFtI921hqD+Ow6x7pOiGcVf4vZMtkxGF
LnYc9XcUlzIruuuS+7sVM44CXF1QGh7bhU71TJtcCkLv8+D7z2RUHbLG6geJzxX3
1OImd23ICDO1oOT5J+ez
=kcB3
-----END PGP SIGNATURE-----
