-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: ocamlbuild
Binary: ocamlbuild
Architecture: any
Version: 0.14.0-1build3~bpo8+1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders: Ximin Luo <infinity0@debian.org>
Homepage: https://github.com/ocaml/ocamlbuild/
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocamlbuild
Vcs-Git: https://salsa.debian.org/ocaml-team/ocamlbuild.git
Build-Depends: debhelper (>= 10), dh-ocaml, ocaml-nox (>= 4.04.0), ocaml-findlib (>= 1.7.3), libfindlib-ocaml-dev (>= 1.7.3)
Package-List:
 ocamlbuild deb ocaml optional arch=any
Checksums-Sha1:
 be89f02fd406116aa0c334cb4eac53a7974bc6dd 198267 ocamlbuild_0.14.0.orig.tar.gz
 b87807ed717bad1b7a41b14253cc99fb6b6af12b 4336 ocamlbuild_0.14.0-1build3~bpo8+1.debian.tar.xz
Checksums-Sha256:
 87b29ce96958096c0a1a8eeafeb6268077b2d11e1bf2b3de0f5ebc9cf8d42e78 198267 ocamlbuild_0.14.0.orig.tar.gz
 63d2fc275bf5f87379794cda498d6e29f780b4a1f6f0279c53e85a08513a5e4c 4336 ocamlbuild_0.14.0-1build3~bpo8+1.debian.tar.xz
Files:
 a7bf2fe594cd16907807c756b14d501f 198267 ocamlbuild_0.14.0.orig.tar.gz
 e9bc57ec4a1caf8f5de5b9833977c822 4336 ocamlbuild_0.14.0-1build3~bpo8+1.debian.tar.xz
Original-Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtVFwAAoJEJZbtTmX/ZH+IuIP/3DQcygJ32W76CZOk223Ucxy
B3vfmc95LoJJ5Ya2XfpltvwwQkBivLNetNY34mPSJ2vt49/u3vKWpD7RHWfLRtTG
Fw60qvXbiNNzQ6X2hbE8vJjxn+1vMQqXrlH1Uv3GgQ/JifFFdn0asbVoQqLm+vIK
aojfAtZ41TocR7Ncl6H3KkNkjAUOLBMMYmpBBId0eXItnZl6+TiznyWuD1YzSi8A
LT82nYW1u1DxHZ7EXhqMNQtr7AEVeU16EB+hyInYs4RXaaD5Xz24G1HNenZM8I1O
G86mYF7MSy3tQENLNixZY1WPgzCUcbBuqO32gW+ztobW5m+yzHsyzzTMkOFz0tcF
H2XOwhiWFUrx/PMOz0xWfoqX3rzCzN3EwasCBuDjCoAOWTxqtM1ovzFRW031Ej9n
1eRp4zdhPX0jsVwQFiumHKdx5BUVucLGSJwDaePnNhnDDfmy3Uqnn/15RntUxS8L
G0OovH7AlX7PJ2U13OU4h0jiALtkzvnoqN67neZ1vNNfQuuJfIwawqamW4aFJfzo
NK9ynJemyVg1DvksI/+GcAoKT4QwEyQDiO6l8qPsT7qP9KTkhjs1i7LjMkDnNg8i
P71xEx+OklTM77YllNfiZzvAaRblTtgxBQOzIiqL87g1Kyc9+BnHuYnzRTw9Ez2l
usOb4Nyx+zbHx2h4Fy8W
=pWxY
-----END PGP SIGNATURE-----
