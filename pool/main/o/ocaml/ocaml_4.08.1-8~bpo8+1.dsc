-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: ocaml
Binary: ocaml-base-nox, ocaml-base, ocaml-nox, ocaml, ocaml-source, ocaml-interp, ocaml-compiler-libs, ocaml-man
Architecture: any all
Version: 4.08.1-8~bpo8+1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Ralf Treinen <treinen@debian.org>, Stéphane Glondu <glondu@debian.org>, Mehdi Dogguy <mehdi@debian.org>, Ximin Luo <infinity0@debian.org>
Homepage: https://ocaml.org/
Standards-Version: 4.4.1
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml.git
Build-Depends: pkg-config, binutils-dev, libiberty-dev, libncurses5-dev, libx11-dev, zlib1g-dev, dh-ocaml (>= 1.0.0~)
Package-List:
 ocaml deb ocaml optional arch=any
 ocaml-base deb ocaml optional arch=any
 ocaml-base-nox deb ocaml optional arch=any
 ocaml-compiler-libs deb ocaml optional arch=any
 ocaml-interp deb ocaml optional arch=any
 ocaml-man deb ocaml optional arch=all
 ocaml-nox deb ocaml optional arch=any
 ocaml-source deb ocaml optional arch=all
Checksums-Sha1:
 acac364d135b8f50247f0da6ee742752c08081ae 3382960 ocaml_4.08.1.orig.tar.xz
 bd3bc9a89cad7790b5979b0eade90e31bf881415 40696 ocaml_4.08.1-8~bpo8+1.debian.tar.xz
Checksums-Sha256:
 cd4f180453ffd7cc6028bb18954b3d7c3f715af13157df2f7c68bdfa07655ea3 3382960 ocaml_4.08.1.orig.tar.xz
 bcae726de17e9938c0ca803c227e07248917ec2d29fd6f415bfb7b25b70f3cd4 40696 ocaml_4.08.1-8~bpo8+1.debian.tar.xz
Files:
 519f095a563323cdaa68301d58bf099d 3382960 ocaml_4.08.1.orig.tar.xz
 b0a636e3225298ce3e9b535610ef410a 40696 ocaml_4.08.1-8~bpo8+1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtSvYAAoJEJZbtTmX/ZH+8ssP/iUec22C7e8EFG8DGCvCayw0
m/8XsjU5lE9Z99hfxgW9M6bDn5F0k8meW3KD1mSZx8yBtoXVI4cMCCSQhaHRnu+/
kSafqNlNP4BK7rI2/IY/t5i0l3PMK4bkOkbByO+ospeNbABRNrkKIeo54jOA05yX
U79CZw9rriA2kC5VEYu6KpX+bwjPwI8/oM+m9i/yoHpqom5dU+IXeXnEG2Y8vjBz
HxL4FS/nrq5x5669fSkWxNeXmimDgi7uMjyQIQNvfqBHTTlfvXyn/NkX1Xf+sLEg
M7iop4/Hq+1JgVIRVlWkC7JfoAZ1CDcXHbz6VhehNHB9cdM9+XCTcVRtcjtmP4DC
LYVoSuctGqDLLHSvpjqoY8GVeQW9qyT2k/jehu6fIs1Zbjo9UdZPa6bSZGh7dZWN
A+G6GuI8ocp0YvSbf5/ojYWSdYxJY2tiG3Kjh/dqZyimjk2KQ55aBKKkA8ygYf5X
MniEMIlZ/B7y/oBwXPJ6lAyDgQR/tk8GlS9jHyX0pzseIYD+Jhy6fv9nJslvIFkJ
G8C1gouREnyIMlGpuQ8XDgBu7ez+wLQcVnA+f2FzuqjZmsRRzWs7sQhjpNRkcbGA
zo4tb62sm4SBIBgTU1zPnoE/sHP8ZkcHG/Hotfm81DJPrnyL49KTCyaco+aF/dkp
ftN9sZv9tmmRsiFVhHED
=6czu
-----END PGP SIGNATURE-----
