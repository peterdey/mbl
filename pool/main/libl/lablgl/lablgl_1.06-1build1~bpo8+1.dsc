-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: lablgl
Binary: liblablgl-ocaml, liblablgl-ocaml-dev
Architecture: any
Version: 1:1.06-1build1~bpo8+1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders:  Stéphane Glondu <glondu@debian.org>, Mehdi Dogguy <mehdi@debian.org>, Ralf Treinen <treinen@debian.org>, Lifeng Sun <lifongsun@gmail.com>
Homepage: https://github.com/garrigue/lablgl
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/lablgl
Vcs-Git: https://salsa.debian.org/ocaml-team/lablgl.git
Build-Depends: debhelper (>= 10), ocaml (>= 4.00.1), tcl-dev, tk-dev, liblabltk-ocaml-dev, libgl1-mesa-dev | libgl-dev, libglu1-mesa-dev | libglu-dev, freeglut3-dev, x11proto-core-dev, libxmu-dev, libx11-dev, dpkg-dev (>= 1.13.19), docbook-xml, docbook-xsl, libxml2-utils, xsltproc, dh-ocaml (>= 0.9)
Package-List:
 liblablgl-ocaml deb ocaml optional arch=any
 liblablgl-ocaml-dev deb ocaml optional arch=any
Checksums-Sha1:
 cbf0687aace2af6ba4c1d1bb3006a30a28c79a49 567281 lablgl_1.06.orig.tar.gz
 c07ce4fa63a74cf35d6da48eddb1fd7bb7f899d3 11608 lablgl_1.06-1build1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 d11852cbdbd40a14a22b30a218d009033c810de461e5717582f5dbe480296a7a 567281 lablgl_1.06.orig.tar.gz
 8c8e46878d46afa442b8247c7409b837755bc2f9aebb8e4ee115ddbae7db65c2 11608 lablgl_1.06-1build1~bpo8+1.debian.tar.xz
Files:
 8ee7a37b016095c4f7cd066f0ebd4436 567281 lablgl_1.06.orig.tar.gz
 169a43530c3c1413ad31768394581b0f 11608 lablgl_1.06-1build1~bpo8+1.debian.tar.xz
Original-Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtV4IAAoJEJZbtTmX/ZH+I0IQAIbnMFaMB3yoK6nADxILiT6D
FnCIwWbJ5BD2d596a8gEZr1zq+E2jtwZwiykW3AHAu15lv8K6wQM4fCtCuN4d8UE
JZTrAEJsfPW8tGMEW1nBx1XI8493xAMZh4vYt/19EdNIy2QKlqNcyazNFPrIk5zP
0qqYF39lpp19r8d0zNBbAzRa8bU8WYApBGgGUHa4HN2InqN6HoDusmx2QCbD3PFd
BSZu7EPQGUKF+VqgKACb2j5wCLJp9NWyx7Ndc7nLpRhws8xSDmAnPh9ZhwKs7ds1
iz/BYRxncnJSRYjcTf9WE/eG2HG1JnKOaUuZKYi7CS/Ti7DTQB2mcIkixrQ5VYRR
DrQiioFh1ExV3HmU6usI9os79Bca4yAr+bZqTynXYnp3J0RxzbZupZo0K/pFKjCc
ukoOsyBJLXZEZc+83POubjVIURiwbCB/tm6KUn1J6UXUbfXTzUsIXtwArcrDxOEV
oRaOx9uqBun0WPnQtcSh9cZbozN3QUFqsejid0qKgoT5gP1tONlaOLldpvZ432xk
2Kt8nYslHzJNa3/lHD8NvAfhQlxeqesq6bHm31E7/p9reVwfV0xSInIihD9HNkkb
ICcLSMbQxvyN+e6PipEcTYNbKdtJ2LvKGikGjE9d0FnvN73MiJp04kuFbO1dXPhr
wl4gIEZ14vEHHAJO797R
=LXyz
-----END PGP SIGNATURE-----
