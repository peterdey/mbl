-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: lablgtk2
Binary: liblablgtk2-ocaml, liblablgtk2-gl-ocaml, liblablgtk2-gnome-ocaml, liblablgtksourceview2-ocaml, liblablgtk2-ocaml-dev, liblablgtk2-gl-ocaml-dev, liblablgtk2-gnome-ocaml-dev, liblablgtksourceview2-ocaml-dev, liblablgtk2-ocaml-doc
Architecture: any all
Version: 2.18.8+dfsg-1build1~bpo8+1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders:  Ralf Treinen <treinen@debian.org>, Stéphane Glondu <glondu@debian.org>
Homepage: http://garrigue.github.io/lablgtk/
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/lablgtk2
Vcs-Git: https://salsa.debian.org/ocaml-team/lablgtk2.git
Build-Depends: debhelper (>= 10), dh-ocaml (>= 0.9.5~), ocaml-nox (>= 4.00.1), ocaml, liblablgl-ocaml-dev (>= 1.04-3~), libncurses5-dev, libgtk2.0-dev, libgtkgl2.0-dev, libglade2-dev, librsvg2-dev, libgtkspell-dev, libgtksourceview2.0-dev (>= 2.10), ocaml-findlib (>= 1.3.2)
Package-List:
 liblablgtk2-gl-ocaml deb ocaml optional arch=any
 liblablgtk2-gl-ocaml-dev deb ocaml optional arch=any
 liblablgtk2-gnome-ocaml deb ocaml optional arch=any
 liblablgtk2-gnome-ocaml-dev deb ocaml optional arch=any
 liblablgtk2-ocaml deb ocaml optional arch=any
 liblablgtk2-ocaml-dev deb ocaml optional arch=any
 liblablgtk2-ocaml-doc deb doc optional arch=all
 liblablgtksourceview2-ocaml deb ocaml optional arch=any
 liblablgtksourceview2-ocaml-dev deb ocaml optional arch=any
Checksums-Sha1:
 a24d20192ea8a1890f55a1e815718a7727056ea6 808168 lablgtk2_2.18.8+dfsg.orig.tar.gz
 4ab8dc9d75255002958f6def003a6318615a62d3 10616 lablgtk2_2.18.8+dfsg-1build1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 cd7a7f03082806b22f252878ef1dfe18bb598c74e14fca9e1138fadcc4b8ba31 808168 lablgtk2_2.18.8+dfsg.orig.tar.gz
 d25381c4dc75cefa543a5e5ad73abdf7c5d20bdac8f1e0fafab0aa1d86da7126 10616 lablgtk2_2.18.8+dfsg-1build1~bpo8+1.debian.tar.xz
Files:
 a36bb8cb86c028de83ff64eb8af1d99b 808168 lablgtk2_2.18.8+dfsg.orig.tar.gz
 d4a76a90c1565dfdfd99faf81ec16e37 10616 lablgtk2_2.18.8+dfsg-1build1~bpo8+1.debian.tar.xz
Original-Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtXWhAAoJEJZbtTmX/ZH+YcIP/0pD8g3Mfc9XMkF0t8uRIo/o
DAgO8FcqD8IyrZmkO2sFw39/kZbTLVQrhcRTe+OYbEmJ8GISFl63wQ6LFEl2YpdN
y7HQygiwpWKX6SacVuXTNkWT8cSpOj1dCwsUWxv+YV9FZ8ubnjCU6u1ZanW9Lf16
Pa5oFwAmFtxB+hXryweWxk4DCli4udmNGCp+7IGSFjMA30lrGnnN9uW0FjZ8D1yW
iaSG3xDVOnMeZjJpCkxLBb8Fmt6+goVakZkpu6RUxp/A/l3y/I3KUtyT3WNw62WE
WrSlYa9MiaFyS+il6XNQ6PJ/S58P6qOeNOg0YBpE/lG2LJTDtu4bG7TH8bk7CZMs
y6lZ8jix3AxYWf7d93zYgQXZys+yM3HNAGuEyZM46QGGtEQGRbxDTTi8yPWI4w1J
PmAzrWPpAIx86AG5d5QPlUC00uoj7Hy4JrmfaptM+kGsaG74ZbYfVWAUN7PbS3Zq
PKZARmsNnsJUViR6iuDGGTMG3JU7EjQR80qIlqjpViB2TOqZXIr9UPqdgIPyXwt7
LK1ywXYqiLcnbA5RKKklDTam6dFZqA9P3hWC9Udp1SolYfjis6VhqONTT1QQMUIe
aAMZm2gDptobypZYcMnetYTImSbAAPxrz3GEII2ECrr3HOTKE8iz40J3Iw/OfQo7
XTi1ep+WbToT5s/BBL4m
=WFu0
-----END PGP SIGNATURE-----
