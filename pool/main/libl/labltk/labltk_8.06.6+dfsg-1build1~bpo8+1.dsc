-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: labltk
Binary: labltk, liblabltk-ocaml-dev, liblabltk-ocaml
Architecture: any
Version: 8.06.6+dfsg-1build1~bpo8+1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders:  Stéphane Glondu <glondu@debian.org>
Homepage: https://github.com/garrigue/labltk
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/ocaml-team/labltk
Vcs-Git: https://salsa.debian.org/ocaml-team/labltk.git
Build-Depends: debhelper (>= 9), ocaml-nox (>= 4.08), ocaml-findlib, tk-dev, dh-ocaml
Package-List:
 labltk deb ocaml optional arch=any
 liblabltk-ocaml deb ocaml optional arch=any
 liblabltk-ocaml-dev deb ocaml optional arch=any
Checksums-Sha1:
 e4504c4ae203da0e36885255ec5d287c6a601206 371466 labltk_8.06.6+dfsg.orig.tar.gz
 d3f5a7541dae1272d38f9446ff5181207b5e98cd 3516 labltk_8.06.6+dfsg-1build1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 bb39f11d577681bce9d6f342708a91baa50650377569c2bc88ce25e3ed973bd6 371466 labltk_8.06.6+dfsg.orig.tar.gz
 0be2bdd11ee817ff943cdbe292bb94f7b66fb4bc4da7fff04296f8863b05c7e3 3516 labltk_8.06.6+dfsg-1build1~bpo8+1.debian.tar.xz
Files:
 7d055f626656907d8a15fc17ac9f1e3b 371466 labltk_8.06.6+dfsg.orig.tar.gz
 3450f92a574e89862b855ab3ea6a8242 3516 labltk_8.06.6+dfsg-1build1~bpo8+1.debian.tar.xz
Original-Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtVttAAoJEJZbtTmX/ZH+WRYP/iFi9DztZZsIXXUB0XgCqNmn
/woX6b0yTngUthzzAqC4FU36FZyHxm0ftPST8mXx0eI0FEEpBQQ958LMxiHzkbe1
ZscG6XWlpDA2bQTKak5ou36Bn3D15KW9qD2j/KjIZyrVsVs8WNORMWLzCBp9t6nY
GAq5HfvW0s9cQouIlKWHHbU++Pn2cS6L9Gu3uDiWFM7QJvCCoTsa6TQ4iM+CUjHT
iQtPqYc3WWojO5ZqbLSgae/2n5I+UKgp3ndRhu0/CTqsoHL00tSDjVUiHhtEv3fJ
RZ4VPOYs1ZvmhnDeJQ12mnpLC5F8Ak7ZiZBou876KU9hMwCB+2xCQ9LHw8SoPIzx
/zCfLfMeoeN+wd0rzQArkZE0bwa0//IbsNq9ec4gBjApsOsss0FyAAPz96inpzpw
CtqRfGFrMhx10XyDDFoXp9CYsPgxzzvuDumroijFn1ECVvJ6RZ+B4lmnS1xrnJsi
0BhAAyj/NqfB98s16ahrYyOONhOaVxqL30P2kwWms4SdVL1IiCx8qPFj1pKMgis0
CKGl5tItDnNxtnB/KnqA76VTYFkSNxL65l3Q5i5MAZ6hXgE00Yz8va8GlpKut6Tg
VIKAFbimiK+xhFpti5P56P1OviPzdcDsj47a6LvAquqqu3elC4ieVzWEOTSbqNFG
xBDEyQUIJllrGVZsSA7h
=5ZQ+
-----END PGP SIGNATURE-----
