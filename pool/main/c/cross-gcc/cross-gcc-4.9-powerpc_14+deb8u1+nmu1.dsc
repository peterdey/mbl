-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: cross-gcc-4.9-powerpc
Binary: cpp-4.9-powerpc-linux-gnu, gcc-4.9-powerpc-linux-gnu, g++-4.9-powerpc-linux-gnu, gfortran-4.9-powerpc-linux-gnu
Architecture: amd64
Version: 14+deb8u1+nmu1
Maintainer: Debian Cross-Toolchain Team <debian-toolchain@lists.debian.org>
Uploaders: Wookey <wookey@debian.org>, Dima Kogan <dima@secretsauce.net>
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=crosstoolchain/cross-gcc.git;a=summary
Vcs-Git: https://alioth.debian.org/anonscm/git/crosstoolchain/cross-gcc.git
Build-Depends: debhelper, cross-gcc-dev, gcc-4.9-source (= 4.9.2-10), libc6-dev:powerpc, linux-libc-dev:powerpc, libgcc1:powerpc, binutils-powerpc-linux-gnu, bison, flex, libtool, gdb, sharutils, netbase, libcloog-isl-dev (>= 0.18), libmpc-dev, libmpfr-dev, libgmp-dev (>= 2:5.0.1~), systemtap-sdt-dev, autogen, expect, realpath, chrpath, zlib1g-dev, lzma, xz-utils, zip
Package-List:
 cpp-4.9-powerpc-linux-gnu deb devel extra arch=amd64
 g++-4.9-powerpc-linux-gnu deb devel extra arch=amd64
 gcc-4.9-powerpc-linux-gnu deb devel extra arch=amd64
 gfortran-4.9-powerpc-linux-gnu deb devel extra arch=amd64
Checksums-Sha1:
 baa0af211eb2cde8a36cc2da6e3fc8a8f7084754 3304 cross-gcc-4.9-powerpc_14+deb8u1+nmu1.tar.xz
Checksums-Sha256:
 7f8e43b0bcf6ec644a46e54d495f801a21d61d010167ab28dc5e7a05f6f4dcab 3304 cross-gcc-4.9-powerpc_14+deb8u1+nmu1.tar.xz
Files:
 9ed0b7435fbd0607ca4b0817b1598d59 3304 cross-gcc-4.9-powerpc_14+deb8u1+nmu1.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjs/uJAAoJEJZbtTmX/ZH+J84P/RkP4cy2W0rsP80AQbC4IIL/
EhBGnna3Cta8a3H7tleU5dx5dt3LkCBGXYb/I4aLyKoLJHndRfys36KJkPmQeLwA
O8gA7ZKJOAQrjmJe7AXWv5+m1jPvIcWamhHfb3YpQzeiCjvM3e9RGyPP1175qw4w
mDWjEdRD4U45AHtpdDMTdkMm81MndBVyX9G0IK5orYt5BQjOhcwqzMq6UUM6iQkZ
s8gmvyeiDwSDkL87VSoKQT47iYhv+JWuyENDoQ2HnqUHWIcXa6eDL9M5Gxtw37CW
2KeE8iw49fJLrfULjCfnbioJBCejNWBJ+65TsZCofRvQ3+wFXHevWAviR81MtEgL
a2oeluailJ4DhCfueecJ9Kc0vY2nERy2LlcHRVTL89+9QF/Yw7lCTiVgszN33BGH
rJL5P64o+hlh6yxTF+Z0evlDJQ+NN04i3i4j0YDVRsUY/mHcpcQNpSaHLB6BCmqB
PGWlIRBrniX90kXuOthVnX65/5f3+f1dbiKAOoTBptGXyebobVTbGn7KVxUNqETh
9nUGpDrII/cALuf26rlLZoXl+4ySB4CUDoQ+Nqr+Fi7ozwgPeBb5m5UU/gP3Q2hB
vKvHiIx5pTgovbNWz8VQ1rZb2E5v2CgntjcbNHpzVhwF/O4v4HLO4rAfTHmqPCR2
JXU3EOEM9SvKxwgkFPHl
=IYZs
-----END PGP SIGNATURE-----
