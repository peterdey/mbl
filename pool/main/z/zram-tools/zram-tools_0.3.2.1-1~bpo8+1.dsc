-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: zram-tools
Binary: zram-tools
Architecture: all
Version: 0.3.2.1-1~bpo8+1
Maintainer: Jonathan Carter <jcc@debian.org>
Homepage: https://github.com/highvoltage/zram-tools
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/debian/zram-tools
Vcs-Git: https://salsa.debian.org/debian/zram-tools.git
Build-Depends: debhelper (>= 10)
Package-List:
 zram-tools deb utils optional arch=all
Checksums-Sha1:
 7471ab8508b26dd9515e0c191ca3e7098b11d991 3421 zram-tools_0.3.2.1.orig.tar.gz
 574eceafc81c3b67c29cfbc08d99280d75331865 2000 zram-tools_0.3.2.1-1~bpo8+1.debian.tar.xz
Checksums-Sha256:
 dc4dd0baf010c62d14436cbbe9ccf2ef84a37250fff5285145b0bb0eef097df8 3421 zram-tools_0.3.2.1.orig.tar.gz
 7d48d82307257b0479153b90a2239a40ee0f4a503578f43031b4d161fe552e0e 2000 zram-tools_0.3.2.1-1~bpo8+1.debian.tar.xz
Files:
 ca0a6ee3dc4db64fb3f35f4b10b64321 3421 zram-tools_0.3.2.1.orig.tar.gz
 dc4f30787e1263679cd825677c678a6f 2000 zram-tools_0.3.2.1-1~bpo8+1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBAgAGBQJjtXQ+AAoJEJZbtTmX/ZH+PuYP/iv+serCqSMEmVoiUTQ4ZyDz
13FTH9Ksu9HxwqBfCYoKOdqkMBXCaoUtvp1tx8NK7ceoUXVb1O0S7A5MjtC76ir9
v3H2G5WLC2ePfFbqxj2fKR+bmKxU1nrlxgRbN2o7LPIcIhfr26piMteGU92Kly53
n869U2K718WDXIwFu+k+TkBnJhqGQeyxDluyWHcSXHEmSs5CW2oisegqPmqmUS9P
6JILQdxsGtvT7KrLIr4fD9JszdNQb8F2BGqLn7M9bvoUW+ocA9TiuvEdV5PC/bUI
OmprpcMi2hrRSXbwCcrtr42ua6lXZP+cYjGBkBFArFBAfTAkr+JzEKAawVpUtBm7
5OTChbfnmQoLiyrjUHv/yR/q/7/frXfR569MC8URRwIVCIaDEHuNSX4Hr1N9+CYi
l+56U5g4/+SFND8+XJBPriMk4tKa+dDyGhP7rzuBcA16S8kn5DCq6Fis/EF78YFF
z1uuDf1aDYyMZ4mJK8tmBzUwFTIHL5wzHFm65exqSy6s3tWuA+UUNVGfCHuVo3Rd
UxkJZtJsL/Xt387Endyy9cDnTy5ffquIZwDalLgLN+0fvckkufWklqH0wbXvUNfs
pAPSD0tBRQtpY6G1L2xknUGuwEqqclQy8w5R88szqw6h6lTzkklWCnk3hvzkJ1wq
cvjefS3Q1Y5Y2OBbZgfM
=KOed
-----END PGP SIGNATURE-----
